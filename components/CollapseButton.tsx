import { ChevronUpIcon, ChevronDownIcon } from "@heroicons/react/24/outline";
import React from "react";
import styles from "../styles/components/CollapseButton.module.css";

type Props = {
  collapsed: boolean;
  onCollapse: React.Dispatch<React.SetStateAction<boolean>>;
};

export const CollapseButton = ({ collapsed, onCollapse }: Props) => {
  return collapsed ? (
    <button
      className={styles.buttonWrapper}
      onClick={() => {
        onCollapse(collapsed);
      }}
    >
      <ChevronDownIcon className={styles.buttonIcon} />
    </button>
  ) : (
    <button
      className={styles.buttonWrapper}
      onClick={() => {
        onCollapse(collapsed);
      }}
    >
      <ChevronUpIcon className={styles.buttonIcon} />
    </button>
  );
};
