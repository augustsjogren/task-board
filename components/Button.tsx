import React, { ButtonHTMLAttributes, PropsWithChildren } from "react";
import styles from "../styles/components/Button.module.css";

export const Button = ({
  flavor,
  children,
  ...props
}: PropsWithChildren<
  {
    flavor: "positive" | "negative";
  } & ButtonHTMLAttributes<HTMLButtonElement>
>) => {
  switch (flavor) {
    case "positive":
      return (
        <button
          {...props}
          className={`${styles.positive} ${props.className}`}
          type={props?.type}
        >
          {children}
        </button>
      );
    case "negative":
      return (
        <button
          {...props}
          className={`${styles.negative} ${props.className}`}
          type={props?.type}
        >
          {children}
        </button>
      );
    default:
      return null;
  }
};
