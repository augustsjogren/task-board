import React, { useState } from "react";
import { Button } from "./Button";
import styles from "../styles/components/EditTaskForm.module.css";

type Props = {
  name: string;
  description: string;
  handleSubmit: (
    e: React.FormEvent<HTMLFormElement>,
    name?: string,
    description?: string
  ) => void;
};

export const EditTaskForm = ({ name, description, handleSubmit }: Props) => {
  const [taskName, setTaskName] = useState<string>();
  const [taskDescription, setTaskDescription] = useState<string>();

  return (
    <form
      className={styles.editForm}
      onSubmit={(e) => handleSubmit(e, taskName, taskDescription)}
    >
      <div className={styles.inputWrapper}>
        <label htmlFor="taskName">Name</label>
        <input
          type="text"
          id="taskName"
          defaultValue={name}
          onChange={(e) => setTaskName(e.target.value)}
        />
      </div>
      <div className={styles.inputWrapper}>
        <label htmlFor="taskName">Description</label>
        <textarea
          rows={5}
          id="taskDescription"
          defaultValue={description}
          onChange={(e) => setTaskDescription(e.target.value)}
        />
      </div>
      <Button className={styles.saveButton} flavor="positive" type="submit">
        Save
      </Button>
    </form>
  );
};
