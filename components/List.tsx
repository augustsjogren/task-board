import React, { useEffect, useMemo, useState } from "react";
import ListItem from "./ListItem";
import styles from "../styles/components/List.module.css";
import useListStorage from "../src/hooks";
import { Task } from "../src/base";
import { ListHeader } from "./ListHeader";
import { AddTaskForm } from "./AddTaskForm";

type Props = {
  id: string;
  onDelete: (id: string) => void;
  filter: string;
};

type Sorting = "asc" | "desc" | "none";

export default function List({ id, onDelete, filter }: Props) {
  const { list, addTask, deleteTask, editTask, editListName } =
    useListStorage(id);

  const [listName, setListName] = useState(list?.name);

  const [sorting, setSorting] = useState<Sorting>("none");

  const [tasks, setTasks] = useState<Array<Task> | undefined>(list?.tasks);

  const deleteList = () => {
    onDelete(id);
  };

  const onAddTask = (
    e: React.FormEvent<HTMLFormElement>,
    name: string,
    description?: string
  ) => {
    e.preventDefault();
    addTask(name, description ?? "");
  };

  const onDeleteTask = (taskId: string) => {
    deleteTask(id, taskId);
  };

  const onEditTask = (taskId: string, name?: string, description?: string) => {
    editTask(taskId, id, name, description);
  };

  const onNameChange = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    editListName(listName ?? "");
  };

  const sortTasks = () => {
    switch (sorting) {
      case "asc":
        setSorting("desc");
        break;
      case "desc":
        setSorting("none");
        break;
      case "none":
        setSorting("asc");
        break;
      default:
        break;
    }
  };

  // Handle task filtering by name
  const filteredTasks = useMemo(() => {
    if (filter && list?.tasks) {
      return list.tasks?.filter((t) =>
        t.name.toLowerCase().includes(filter.toLowerCase())
      );
    } else {
      return list?.tasks;
    }
  }, [filter, list?.tasks]);

  // Handle sorting of the tasks
  useEffect(() => {
    const tasksCopy = [...(filteredTasks || [])];

    if (sorting === "asc") {
      tasksCopy.sort((a, b) =>
        a.name.toLowerCase() > b.name.toLowerCase() ? 1 : 0
      ) || [];
    } else if (sorting === "desc") {
      tasksCopy.sort((a, b) =>
        a.name.toLowerCase() < b.name.toLowerCase() ? 1 : 0
      ) || [];
    }

    setTasks(tasksCopy);
  }, [filteredTasks, sorting]);

  return (
    <div className={styles.container}>
      <ListHeader
        onNameChange={onNameChange}
        listName={listName}
        setListName={setListName}
        deleteList={deleteList}
        sortTasks={sortTasks}
      />

      {tasks?.length ? (
        tasks?.map((task) => (
          <ListItem
            id={task.id}
            key={task.id}
            name={task.name}
            description={task.description}
            onDelete={onDeleteTask}
            onEdit={onEditTask}
          />
        ))
      ) : (
        <p>No tasks in list</p>
      )}
      <AddTaskForm onAddTask={onAddTask} />
    </div>
  );
}
