import React, { useState } from "react";
import styles from "../styles/components/ListItem.module.css";
import { Button } from "./Button";
import { CollapseButton } from "./CollapseButton";
import { EditTaskForm } from "./EditTaskForm";

type Props = {
  id: string;
  name: string;
  description: string;
  onDelete: (id: string) => void;
  onEdit: (id: string, name?: string, description?: string) => void;
};

export default function ListItem({
  name,
  description,
  id,
  onEdit,
  onDelete,
}: Props) {
  const [editMode, setEditMode] = useState(false);

  const handleSubmit = (
    e: React.FormEvent<HTMLFormElement>,
    taskName?: string,
    taskDescription?: string
  ) => {
    e.preventDefault();
    onEdit(id, taskName, taskDescription);
    setEditMode(false);
  };

  return (
    <div className={styles.container}>
      <div className={styles.readContent}>
        <div className={styles.taskContent}>
          <h3>{name}</h3>
          <div>{description}</div>
        </div>

        <div className={styles.buttonContainer}>
          <CollapseButton collapsed={!editMode} onCollapse={setEditMode} />
        </div>
      </div>

      {editMode && (
        <>
          <hr />
          <EditTaskForm
            name={name}
            description={description}
            handleSubmit={handleSubmit}
          />
          <Button flavor="negative" onClick={() => onDelete(id)}>
            Delete task
          </Button>
        </>
      )}
    </div>
  );
}
