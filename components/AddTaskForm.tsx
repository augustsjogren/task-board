import { PlusIcon } from "@heroicons/react/24/outline";
import React, { useState } from "react";
import styles from "../styles/components/AddTaskForm.module.css";
import { Button } from "./Button";

type Props = {
  onAddTask: (
    e: React.FormEvent<HTMLFormElement>,
    name: string,
    description?: string
  ) => void;
};

export const AddTaskForm = ({ onAddTask }: Props) => {
  const [addTaskMode, setAddTaskMode] = useState(false);
  const [taskName, setTaskName] = useState("");
  const [taskDescription, setTaskDescription] = useState("");

  return (
    <>
      <hr className={styles.separator} />
      {!addTaskMode ? (
        <button
          className={styles.addButtonWrapper}
          onClick={() => setAddTaskMode(!addTaskMode)}
        >
          <PlusIcon className={styles.buttonIcon} /> Add task
        </button>
      ) : (
        addTaskMode && (
          <form
            onSubmit={(e) => {
              onAddTask(e, taskName, taskDescription);
              setAddTaskMode(false);
              setTaskName("");
              setTaskDescription("");
            }}
          >
            <div className={styles.inputWrapper}>
              <label htmlFor="taskName">Name</label>
              <input
                type="text"
                id="taskName"
                onChange={(e) => setTaskName(e.target.value)}
                value={taskName}
                required
              />
            </div>
            <div className={styles.inputWrapper}>
              <label htmlFor="taskDescription">Description</label>
              <input
                type="text"
                id="taskDescription"
                onChange={(e) => setTaskDescription(e.target.value)}
                value={taskDescription}
              />
            </div>
            <div className={styles.addButtonContainer}>
              <Button flavor="positive" type="submit">
                Add
              </Button>
              <button onClick={() => setAddTaskMode(false)}>Cancel</button>
            </div>
          </form>
        )
      )}
    </>
  );
};
