import {
  CheckIcon,
  PencilIcon,
  ArrowsUpDownIcon,
  TrashIcon,
} from "@heroicons/react/24/outline";
import React, { useState } from "react";
import styles from "../styles/components/ListHeader.module.css";

type Props = {
  onNameChange: (e: React.FormEvent<HTMLFormElement>) => void;
  listName?: string;
  setListName: React.Dispatch<React.SetStateAction<string | undefined>>;
  deleteList: () => void;
  sortTasks: () => void;
};

export const ListHeader = ({
  listName,
  onNameChange,
  setListName,
  deleteList,
  sortTasks,
}: Props) => {
  const [editMode, setEditMode] = useState(false);

  return (
    <form onSubmit={(e) => onNameChange(e)}>
      <div className={styles.listHeader}>
        {editMode ? (
          <input
            type="text"
            name="listName"
            id="listName"
            value={listName}
            onChange={(e) => setListName(e.target.value)}
          />
        ) : (
          <h2>{listName}</h2>
        )}

        <div className={styles.buttonContainer}>
          {editMode ? (
            <button
              type="submit"
              className={styles.deleteButtonWrapper}
              onClick={() => setEditMode(!editMode)}
            >
              <CheckIcon className={styles.buttonIcon} />
            </button>
          ) : (
            <button
              className={styles.deleteButtonWrapper}
              onClick={() => setEditMode(!editMode)}
            >
              <PencilIcon className={styles.buttonIcon} />
            </button>
          )}
          <button
            className={styles.deleteButtonWrapper}
            onClick={() => sortTasks()}
          >
            <ArrowsUpDownIcon className={styles.buttonIcon} />
          </button>
          <button className={styles.deleteButtonWrapper} onClick={deleteList}>
            <TrashIcon className={styles.buttonIcon} />
          </button>
        </div>
      </div>
    </form>
  );
};
