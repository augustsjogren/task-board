import useLocalStorageState from "use-local-storage-state";
import { ListType, Task } from "./base";
import { v4 as uuidv4 } from "uuid";
import { useCallback } from "react";

/**
 * Provides access to a list from the localstorage for CRUD operations.
 * @param listId Id of the list to CRUD
 */
function useListStorage(listId: string) {
  const [lists, setLists] = useLocalStorageState<Array<ListType>>("lists");

  let list: ListType | undefined = undefined;

  if (lists) {
    list = lists.find((l) => l.id === listId);
  }

  const addTask = useCallback(
    (name: string, description: string) => {
      const listsCopy = [...(lists || [])];

      const newTask: Task = { id: uuidv4(), name, description };

      const listToModify = listsCopy.find((l) => l.id === listId);
      listToModify?.tasks.push(newTask);

      setLists(listsCopy);
    },
    [listId, lists, setLists]
  );

  const deleteTask = useCallback(
    (listId: string, taskId: string) => {
      const listsCopy = [...(lists || [])];

      const listToModify = listsCopy.find((l) => l.id === listId);

      if (listToModify?.tasks) {
        listToModify.tasks = listToModify?.tasks.filter((t) => t.id !== taskId);
      }

      setLists(listsCopy);
    },
    [lists, setLists]
  );

  const editTask = useCallback(
    (taskId: string, listId: string, name?: string, description?: string) => {
      const listsCopy = [...(lists || [])];

      const listToModify = listsCopy.find((l) => l.id === listId);

      if (listToModify?.tasks) {
        const taskToEdit = listToModify.tasks.find((t) => t.id === taskId);

        if (taskToEdit) {
          if (name !== undefined) {
            taskToEdit.name = name;
          }

          if (description !== undefined) {
            taskToEdit.description = description;
          }
        }

        setLists(listsCopy);
      }
    },
    [lists, setLists]
  );

  const editListName = useCallback(
    (name: string) => {
      const listsCopy = [...(lists || [])];

      const listToRename = listsCopy.find((l) => l.id === listId);

      if (listToRename) {
        listToRename.name = name;
      }

      setLists(listsCopy);
    },
    [listId, lists, setLists]
  );

  const setTasks = useCallback(
    (tasks: Task[]) => {
      const listsCopy = [...(lists || [])];

      const listToModify = listsCopy.find((l) => l.id === listId);

      if (listToModify) {
        listToModify.tasks = tasks;
      }

      setLists(listsCopy);
    },
    [listId, lists, setLists]
  );

  return { list, addTask, deleteTask, editTask, editListName, setTasks };
}

export default useListStorage;
