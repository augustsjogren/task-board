/**
 * Here are common ts elements such as types stored
 */

export type Task = {
  id: string;
  name: string;
  description: string;
};

export type ListType = {
  name: string;
  description: string;
  id: string;
  tasks: Task[];
};
