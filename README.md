# Task board

This is a task board application to help keep all of your tasks organized. The application supports multiple lists with an arbitrary number of tasks each.

The application stores its data in localstorage, to make the data persistent between tabs and browser sessions.

## Prerequisites

You will need to have `node.js` installed, `>= v.14.x` is recommended.

## Getting started

To get started first clone this repository:

```
$ git clone git@gitlab.com:augustsjogren/task-board.git
```

or

```
$ git clone https://gitlab.com/augustsjogren/task-board.git
```

Then move into the directory:

```
$ cd task-board
```

To install all dependencies, run the following command:

```
$ npm install
```

Then execute the following command to start the application in development mode:

```
$ npm run dev
```

If you rather would like to run the app in production mode, here is how you would do that:

```
$ npm run build
$ npm run start
```

The application should now be running on port `localhost:3000`. If not, check the terminal to see which port was used.

At last, enjoy the application!
