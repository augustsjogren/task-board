import type { NextPage } from "next";
import Head from "next/head";
import { FormEvent, useState } from "react";
import List from "../components/List";
import styles from "../styles/Home.module.css";
import { v4 as uuidv4 } from "uuid";
import useLocalStorageState from "use-local-storage-state";
import { Button } from "../components/Button";
import { ListType } from "../src/base";

const Home: NextPage = () => {
  const [lists, setLists] = useLocalStorageState<Array<ListType>>("lists");
  const [listName, setListName] = useState("");

  const [filter, setFilter] = useState("");

  // Add a new list
  const onSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const newList: ListType = {
      description: "",
      id: uuidv4(), // This should be done on DB level
      name: listName,
      tasks: [],
    };

    setListName("");

    setLists([...(lists ?? []), newList]);
  };

  const deleteList = (id: string) => {
    if (lists) {
      const filteredLists = lists.filter((l) => l.id !== id);
      setLists(filteredLists);
    }
  };

  const onNameChange = (value: string) => {
    setListName(value);
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>Task Board</title>
        <meta name="description" content="Task board by August" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <header>
        <h1 className={styles.title}>Task Board</h1>
        <div className={styles.headerFormWrapper}>
          <form onSubmit={(e) => onSubmit(e)} className={styles.addListForm}>
            <label htmlFor="addList">Add a list</label>
            <input
              type="text"
              placeholder="List name"
              id="addList"
              required
              value={listName}
              onChange={(e) => onNameChange(e.target.value)}
            />
            <Button flavor="positive" type="submit">
              Add list
            </Button>
          </form>
          <div className={styles.filterWrapper}>
            <label htmlFor="filter">Filter tasks</label>
            <input
              type="text"
              name="filter"
              id="filter"
              className={styles.filterInput}
              placeholder="Filter tasks"
              onChange={(e) => setFilter(e.target.value)}
            />
          </div>
        </div>
      </header>
      <main className={styles.main}>
        <div className={styles.listContainer}>
          {(lists ?? []).map((l) => (
            <List id={l.id} key={l.id} onDelete={deleteList} filter={filter} />
          ))}
        </div>
      </main>

      <footer className={styles.footer}>
        <span>By: August Sjögren</span>
      </footer>
    </div>
  );
};

export default Home;
